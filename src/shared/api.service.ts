import { Injectable, HttpService } from '@nestjs/common';

@Injectable()
export class ApiService {

  constructor(private readonly http: HttpService) {}

    async get(url: string): Promise<any> {
      const result = await this.http.get(url).toPromise()
        .then(res => res.data)
        .catch(err => console.log(`Error when requesting to ${url}, \r\n Error: ${JSON.stringify(err)}`))
      return result;
    }
}
