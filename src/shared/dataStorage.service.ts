import * as NodeCache from 'node-cache';

import { Injectable } from '@nestjs/common';

@Injectable()
export class DataStorageService {
  private cache: NodeCache;

  constructor() {
    this.cache = new NodeCache( { stdTTL: 100, checkperiod: 120 } );
  }
  
  set(key: string, obj: any) {
    this.cache.set(key, obj);

  }

  get(key) {
    const value = this.cache.get(key);
    
    return value;
  }
}