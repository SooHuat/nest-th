import { CommentDto } from "src/comments/dto/comment.dto";

export class PostDto {
  postId: number;
  postTitle: string;
  postBody: string;
  totalNumberOfComments: number;
  comments: CommentDto[];
}
