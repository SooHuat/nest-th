import { Injectable, HttpService } from '@nestjs/common';
import { PostDto } from './dto/post.dto';
import { DataStorageService } from '../shared/dataStorage.service';
import { ApiService } from 'src/shared/api.service';

@Injectable()
export class PostsService {

  constructor(private readonly apiService: ApiService,
    private readonly dataStorageService: DataStorageService) {}
  
  async findAll(): Promise<PostDto[]> {
    const baseUrl = "https://jsonplaceholder.typicode.com/";
    const token = "posts";
    const cachedPosts = this.dataStorageService.get(token);
    if (cachedPosts) {
      return cachedPosts as PostDto[];
    }
    const url = baseUrl + token;
    const posts = await this.apiService.get(url);
    return posts;
  }
  
  async find(id: string): Promise<PostDto> {
    const baseUrl = "https://jsonplaceholder.typicode.com/";
    const token = `posts/${id}`;
    const cachedPosts = this.dataStorageService.get(token);
    if (cachedPosts) {
      return cachedPosts as PostDto;
    }
    const url = baseUrl + token;
    const posts = await this.apiService.get(url);
    return posts;
  }
}