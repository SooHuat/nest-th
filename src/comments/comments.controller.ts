import { Controller, Get, Query } from '@nestjs/common';
import * as _ from 'lodash';
import { CommentsService } from '../comments/comments.service';
import { CommentDto } from './dto/comment.dto';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Get()
  async findAll(@Query() query: CommentDto): Promise<CommentDto[]> {
    let comments: CommentDto[] = await this.commentsService.findAll();
    for (const key in query) {
      const value = query[key];
      if (!key || !value) {
        continue;
      }
      comments = _.filter(comments, comment => {
        const commentValue = comment[key];
        return commentValue && commentValue == value || (typeof commentValue == "string" && commentValue.indexOf(value) != -1);
      });
    }
    return comments;
  }
}